#pragma once
#include "help_ex.h"
#include "ExDUIR_Func.h"

void _object_init();
void pfnDefaultFreeData(void* dwData);
bool Flag_Query(int dwFlag);
void Flag_Add(int dwFlag);
void Flag_Del(int dwFlag);